package com.example.demo;


import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.*;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class DojoReactiveTest {

    @Test
    void converterData(){
        List<Player> list = CsvUtilFile.getPlayers();
        assert list.size() == 18207;
    }

    @Test
    void jugadoresMayoresA35() {
        List<Player> list = CsvUtilFile.getPlayers();
        Flux<Player> playerFlux = Flux.fromIterable(list);

        playerFlux
                .filter(player -> player.getAge() > 35)
                .collectList()
                .map(jugadorList -> {
                    long size = jugadorList.size();
                    Player primer = jugadorList.get(0);
                    Player ultimo = jugadorList.get((int) (size - 1));
                    System.out.println("Size: " + size);
                    System.out.println("Primer jugador: \n"+ primer + "\nSegundo jugador\n" + ultimo);
                    return jugadorList;
                });//.subscribe();
    }


@Test
    void jugadoresMayoresA35SegunClub(){
        List<Player> list = CsvUtilFile.getPlayers();
        Flux<Player> observable = Flux.fromIterable(list);

        observable
                .filter(player -> player.getAge() > 35)
                .distinct()
                .groupBy(Player::getClub)
                .flatMap(grouped -> grouped
                        .collectList()
                        .map(playerList -> {
                            Map<String, List<Player>> map = new HashMap<>();
                            map.put(grouped.key(), playerList);
                            return map;
                        }))
                .subscribe(group -> {
                    group.forEach((key, value) -> {
                        System.out.println("\n");
                        System.out.println(key + ": ");
                        value.forEach(System.out::println);
                    });
                });

    }


    @Test
    void mejorJugadorConNacionalidadFrancia(){
        Predicate<Player> countryPredicate = player -> player.getNational().equals("France");
        Flux.fromIterable(CsvUtilFile.getPlayers())
                .filter(countryPredicate)
                .reduce((player, player2) -> player.getWinners() / player.getGames() >
                        player2.getWinners() / player2.getGames() ? player : player2)
                .subscribe(System.out::println);
    }

    @Test
    void clubsAgrupadosPorNacionalidad(){
        Flux.fromIterable(CsvUtilFile.getPlayers())
                .distinct()
                .collectMultimap(Player::getNational, Player::getClub)
                .subscribe(clubsMap -> {
                    clubsMap.forEach((key, value) -> {
                        System.out.println("++++++++++++++++++");
                        System.out.println(key);
                        System.out.println(value);
                    });
                });
    }

    @Test
    void clubConElMejorJugador(){
        Flux.fromIterable(CsvUtilFile.getPlayers())
                .reduce((player1, player2) -> player1.getWinners() / player1.getGames() >
                        player2.getWinners() / player2.getGames() ? player1 : player2)
                .subscribe(player -> System.out.println(player.getClub()));
    }

    @Test
    void ElMejorJugador() {
        Flux.fromIterable(CsvUtilFile.getPlayers())
                .reduce((player1, player2) -> player1.getWinners() / player1.getGames() >
                        player2.getWinners() / player2.getGames() ? player1 : player2)
                .subscribe(System.out::println);
    }

    @Test
    void mejorJugadorSegunNacionalidad(){
        Flux.fromIterable(CsvUtilFile.getPlayers())
                .groupBy(Player::getNational)
                .flatMap(club ->
                        club.reduce((player1, player2) -> player1.getWinners() > player2.getWinners() ? player1 : player2))
                .collectMultimap(Player::getNational, Player::getName)
                .subscribe(clubsMap -> {
                    clubsMap.forEach((key, value) -> {
                        System.out.println("++++++++++++++++++");
                        System.out.println(key);
                        System.out.println(value);
                    });
                });
    }



}
