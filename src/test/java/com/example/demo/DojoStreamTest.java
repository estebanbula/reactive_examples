package com.example.demo;


import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;

import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.stream.Collectors;

public class DojoStreamTest {

    @Test
    void converterData(){
        List<Player> list = CsvUtilFile.getPlayers();
        assert list.size() == 18207;
    }

    @Test
    void jugadoresMayoresA35(){
        List<Player> players = CsvUtilFile.getPlayers();
        Set<Player> result = players.stream()
                .filter(player -> player.getAge() > 35)
                .collect(Collectors.toSet());
        result.forEach(System.out::println);
    }

    @Test
    void jugadoresMayoresA35SegunClub(){
        List<Player> list = CsvUtilFile.getPlayers();
        var result = list.stream().filter(player -> player.getAge() > 35)
                .collect(Collectors.groupingBy(Player::getClub));

        System.out.println(result);
    }

    @Test
    void mejorJugadorConNacionalidadFrancia(){
        CsvUtilFile.getPlayers().stream()
                .filter(player -> player.getNational().equals("France"))
                .reduce((player, player2) -> player.getWinners() > player2.getWinners() ? player : player2)
                .ifPresent(System.out::println);
    }


    @Test
    void clubsAgrupadosPorNacionalidad(){
        CsvUtilFile.getPlayers().stream()
                .collect(Collectors.groupingBy(Player::getNational, HashMap::new,
                        Collectors.mapping(Player::getClub, Collectors.toSet())))
                .forEach((key, value) -> {
                    System.out.println("+++++++++++++++");
                    System.out.println(key);
                    System.out.println(value);
                });
    }

    @Test
    void clubConElMejorJugador(){
        CsvUtilFile.getPlayers().stream()
                .reduce((player, player2) -> player.getWinners() > player2.getWinners() ? player : player2)
                .ifPresent(player -> System.out.println(player.getClub()));
    }

    @Test
    void ElMejorJugador(){
        CsvUtilFile.getPlayers().stream()
                .reduce((player, player2) -> player.getWinners() > player2.getWinners() ? player : player2)
                .ifPresent(player -> System.out.println(player.getName()));
    }

    @Test
    void mejorJugadorSegunNacionalidad(){
        CsvUtilFile.getPlayers().stream()
                .collect(Collectors.groupingBy(Player::getNational,
                        Collectors.reducing(
                                (player, player2) -> player.getWinners() > player2.getWinners() ? player : player2)))
                .forEach((key, value) -> {
                    System.out.println("+++++++++++++++");
                    System.out.println(key);
                    System.out.println(value);
                });
    }


}
